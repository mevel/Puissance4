(******************************************************************************)
(*               Puissance 4                                                  *)
(*                                                               Glen Mével   *)
(*                                                                            *)
(*    Des tests pour les différents modules                                   *)
(*                                                                            *)
(******************************************************************************)

open Regles
open Affiche
open Puissance4

let pp = Printf.printf

let partie () =
  let jeu = initJeu () in
  let _ = addJeton jeu 2 Rouge in
  let _ = addJeton jeu 2 Jaune in
  let _ =   addJeton jeu 2 Rouge in
  let _ =   addJeton jeu 2 Jaune in
  let _ =   addJeton jeu 3 Rouge in
  let _ =   addJeton jeu 4 Jaune in
  let _ =   addJeton jeu 3 Rouge in
  let _ =   addJeton jeu 3 Rouge in
  jeu

let t1 () =
  let jeu = partie () in
  let yR = addJeton jeu 3 Rouge in
  let yJ = addJeton jeu 5 Jaune in
  begin
    pp "%i\n\n" yR;
    if (estGagnant jeu Jaune 5 yJ)
    then pp "Jaune a gagné\n";
    if (estGagnant jeu Rouge 3 yR)
    then pp "Rouge a gagné\n";
  end

let t2 () =
  initGraph ();
  let jeu = partie () in
  printJeu jeu;
  pause ()

let t3 () =
  let jeu = initJeu () in
  initGraph ();
  let x = getCoupHumain jeu in
  pp "Colonne pressée: %i" x

let () =
  (*t1();*)
  (*t2();*)
  (*t3();*)
  ()
