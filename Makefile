##################################
# MAKEFILE DU PROJET PUISSANCE 4 #
##################################

# Ne faites pas attention à la ligne suivante (nécessaire pour les machines de la 411)
LIBS=graphics.cma -cclib -lgraphics -cclib -L/usr/X11R6/lib -cclib -lX11

# modules du projet par ordre de dépendance :
MODULES=regles, ia, affiche, puissance4,
CMO=$(MODULES:,=.cmo)

SRC=$(wildcard *.ml *mli)

# règle pour compiler tout le jeu :
all: jeu.out
jeu.out: $(CMO)

# règle pour compiler tous les tests :
tests: tests.out
tests.out: $(CMO) tests.cmo

%.out:
	ocamlc -o $@ $(LIBS) $^

%.cmo: %.ml
	ocamlc -c $<

%.cmi: %.mli
	ocamlc -c $<
# ne pas supprimer les fichiers .cmi, considérés comme intermédiaires par make :
.PRECIOUS: %.cmi

# règle pour nettoyer le dossier des fichiers de compilation :
clean:
	rm -f *.cm[io] *~ .*~
	rm -f *.out

scratch: clean all

# génération automatique des dépendances entres modules :
Makefile.depend: $(SRC)
	ocamldep *.mli *.ml > $@
include Makefile.depend
