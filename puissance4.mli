(******************************************************************************)
(*               Puissance 4                                                  *)
(*                                                               Glen Mével   *)
(*                                                                            *)
(*    Session interactive permettant de jouer                                 *)
(*                                                                            *)
(******************************************************************************)

(* Récupère une touche pressée pendant que la fenêtre graphique a le focus,
   teste si la touche correspond à une colonne non vide et la renvoie
   dans ce cas. Sinon, attend une autre touche.
*)
val getCoupHumain: Regles.jeu -> int

(* Type encodant la façon d’émettre les coups pour un joueur donné. *)
type sourceCoups =
  | Humain
  | IA

(* fonction permettant de récupérer le prochain coup :
   renvoie un entier entre 1 et sizeX pour sélectionner
   une colonne ou 0 pour arrêter la partie. *)
val getCoup: Regles.jeu -> sourceCoups -> int

(* Exceptions marquant la fin de la boucle interactive. *)
exception FinJeu of Regles.case (* indique qui a gagné ou s’il y a match nul. *)
exception Interrompu

(* Fonction implémentant la boucle interactive. *)
val jouer: sourceCoups -> sourceCoups -> unit

(* Fonction principale. *)
val main: unit -> unit
