(******************************************************************************)
(*               Puissance 4                                                  *)
(*                                                               Glen Mével   *)
(*                                                                            *)
(*    Des tests pour les différents modules                                   *)
(*                                                                            *)
(******************************************************************************)

(* Crée une partie un peu avancée *)
val partie: unit -> Regles.jeu

(* Teste la fonction estGagnant *)
val t1: unit -> unit

(* Teste l'affichage *)
val t2: unit -> unit

(* Teste getCoup *)
val t3: unit -> unit
