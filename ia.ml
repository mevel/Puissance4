(******************************************************************************)
(*               Puissance 4                                                  *)
(*                                                               Glen Mével   *)
(*                                                                            *)
(*    Algorithme implémenté par le joueur Ordinateur                          *)
(*                                                                            *)
(******************************************************************************)

(** FONCTIONS AUXILIAIRES DÉFINIES DANS MON “regles.ml” RAPPELÉES ICI
 ** POUR LA COMPATIBILITÉ DE CE “ia.ml” AVEC D’AUTRES IMPLÉMENTATIONS. **)

module Regles
= struct

    include Regles

    let hauteurColonne jeu x =
      let y = ref 0 in
      while !y < sizeY && jeu.pl.(x).(!y) <> Noir do
        incr y
      done;
      !y

    let nombreJetons jeu =
      let s = ref 0 in
      for x = 0 to sizeX-1 do
        s := !s + hauteurColonne jeu x
      done;
      !s

    let cancelJeton jeu x y =
      jeu.pl.(x).(y) <- Noir

    let joueurAdverse jeu =
      match jeu.tour with
      | Jaune -> Rouge
      | Rouge -> Jaune

    let tourSuivant jeu =
      jeu.tour <- joueurAdverse jeu

end


(** DÉBUT DU MODULE D’IA. **)

open Regles

let profMinMax = 6              (* profondeur maximale pour min-max *)
let profAlphaBeta = 7           (* profondeur maximale pour alpha-bêta *)

let scoreInfini = 1000000
let scoreVictoire = scoreInfini
let scoreDefaite = -scoreVictoire

exception CoupQuiTue

(* Renvoie la taille du plus grand alignement de jetons du [joueur] passant par
 * la case ([x],[y]), sans compter la case elle-même. *)
let potentiel jeu joueur x y =
  (* On va compter l’alignement maximal de pions passant par le pion joué
     et selon chaque direction. *)
  let count (a,b) =
    let k = ref 1 in
    while   0 <= !k*a+x  &&  !k*a+x < sizeX
        &&  0 <= !k*b+y  &&  !k*b+y < sizeY
        &&  jeu.pl.(!k*a+x).(!k*b+y) = Jeton joueur do
      incr k
    done;
    !k-1
  in
  (* horizontalement *)           count (-1, 0) + count (1, 0)
  (* verticalement   *) |> max @@ count ( 0,-1) (* C’est forcément vide au-dessus. *)
  (* oblique \       *) |> max @@ count (-1, 1) + count (1,-1)
  (* oblique /       *) |> max @@ count (-1,-1) + count (1, 1)

(* fonction d’évaluation
 * On suppose que la position n’est pas gagnée ou perdue, les victoires immédiates
 * étant gérées par la fonction d’exploration.
 * Pour chaque colonne i, on calcule la taille p_i du plus grand alignement de jetons
 * passant par la première case libre de cette colonne (sans compter la case elle-même),
 * le score est alors la somme des p_i² moins la même somme pour l’adversaire.
 * On détecte aussi des combinaisons gagnantes (gain immédiat pour soi ou gain par
 * double menace pour l’adversaire), et on les traite de façon prioritaire. *)
let eval jeu =
  try
    let joueur = jeu.tour
    and adversaire = joueurAdverse jeu in
    let menaceAdverse = ref 0
    and somme = ref 0
    and sommeAdverse = ref 0 in
    for x = 0 to sizeX-1 do
      let y = hauteurColonne jeu x in
      if y < sizeY then begin
        let p = potentiel jeu joueur x y in
        if p+1 >= nbV then
          raise CoupQuiTue;
        somme := !somme + p*p;
        let q = potentiel jeu adversaire x y in
        if q+1 >= nbV then begin
          incr menaceAdverse;
          if y+1 < sizeY && potentiel jeu adversaire x (y+1) + 1 > nbV then
            incr menaceAdverse
        end;
        sommeAdverse := !sommeAdverse + q*q
      end
    done;
    if !menaceAdverse > 1 then
      scoreDefaite
    else
      !somme - !sommeAdverse
  with CoupQuiTue ->
    scoreVictoire

(* algorithme alpha-beta ; renvoie un couple (coup,score). *)
let rec alphabeta jeu p alpha beta =
  if p = 0 then
    (0, eval jeu)
  else begin
    let fin = ref true in    (* indique si le plateau est déjà plein *)
    let coupMax = ref (-1)
    and scoreMax = ref alpha in
    begin try for x = 0 to sizeX-1 do
      if not (isFull jeu x) then begin
        fin := false;
        let y = addJeton jeu x jeu.tour in
        if estGagnant jeu jeu.tour x y then begin
          scoreMax := scoreVictoire;
          coupMax := x
        end else begin
          tourSuivant jeu;
          let (_,scoreMaxAdverse) = alphabeta jeu (p-1) (-beta) (- !scoreMax) in
          tourSuivant jeu;
          let score = -scoreMaxAdverse in
          if score > !scoreMax then begin
            scoreMax := score;
            coupMax := x
          end
        end;
        cancelJeton jeu x y;
        if !scoreMax >= beta then
          raise CoupQuiTue
      end
    done with CoupQuiTue -> () end;
    (!coupMax+1, if !fin then 0 else !scoreMax)
  end

(* algorithme min-max ; renvoie un couple (coup,score). *)
let rec minmax jeu p =
  if p = 0 then
    (0, eval jeu)
  else begin
    let fin = ref true in    (* indique si le plateau est déjà plein *)
    let coupMax = ref (-1)
    and scoreMax = ref (-scoreInfini) in
    begin try for x = 0 to sizeX-1 do
      if not (isFull jeu x) then begin
        fin := false;
        let y = addJeton jeu x jeu.tour in
        if estGagnant jeu jeu.tour x y then begin
          scoreMax := scoreVictoire;
          coupMax := x
        end else begin
          tourSuivant jeu;
          let (_,scoreMaxAdverse) = minmax jeu (p-1) in
          tourSuivant jeu;
          let score = -scoreMaxAdverse in
          if score > !scoreMax then begin
            scoreMax := score;
            coupMax := x
          end
        end;
        cancelJeton jeu x y;
        if !scoreMax = scoreVictoire then
          raise CoupQuiTue
      end
    done with CoupQuiTue -> () end;
    (!coupMax+1, if !fin then 0 else !scoreMax)
  end

(* fonction demandant à l’IA de calculer le prochain coup *)
let getCoupIA jeu =
  (* On augmente la profondeur de l’alpha-bêta au cours de la partie
     (possible car le nombre de branches à explorer diminue) : *)
  let prof = profAlphaBeta + (nombreJetons jeu) / 7 in
  let (coup,_) = alphabeta jeu prof (-scoreInfini) scoreInfini in
  (* Si l’IA détecte qu’elle a perdu si l’adversaire joue parfaitement,
     on joue aléatoirement plutôt que d’abandonner. *)
  if coup = 0 then
    let x = ref 0 in
    while (x := Random.int sizeX; isFull jeu !x) do () done;
    !x + 1
  else
    coup

let meilleurCoup jeu =
  getCoupIA jeu - 1
