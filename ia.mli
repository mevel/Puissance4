(******************************************************************************)
(*               Puissance 4                                                  *)
(*                                                               Glen Mével   *)
(*                                                                            *)
(*    Algorithme implémenté par le joueur Ordinateur                          *)
(*                                                                            *)
(******************************************************************************)

(* Renvoie un entier entre 1 et Regles.sizeX, ou 0 pour abandonner. *)
val getCoupIA  : Regles.jeu -> int

(* Calcule le résultat de MinMax cad. la colonne correspondnat à un "bon coup"
 * (indice entre 0 et Regles.sizeX-1). *)
val meilleurCoup  : Regles.jeu -> int
