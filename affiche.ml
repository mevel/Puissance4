(******************************************************************************)
(*               Puissance 4                                                  *)
(*                                                               Glen Mével   *)
(*                                                                            *)
(*    Affichage du plateau, menu, etc..                                       *)
(*                                                                            *)
(******************************************************************************)

open Regles

let coulFond =    (*Graphics.white*)Graphics.rgb 200 210 210
let coulPlateau = Graphics.rgb 20 0 100
let coulVide =    Graphics.rgb 200 210 210
let coulJaune =   Graphics.yellow
let coulRouge =   Graphics.red
let coulTexte =   Graphics.black
let coulMenuFond =  (*coulVide*)coulPlateau
let coulMenuTexte = (*coulTexte*)Graphics.white

let margeX =   40
let margeY =   40
let caseW =    50
let caseH =    50
let rayonJ =   caseW / 2 - 4
let plateauW = sizeX*caseW
let plateauH = sizeY*caseH
let menuW =    100
let margeMenuTexte = 5
let margeMenuItem =  10

let winW = margeX + plateauW + margeX + menuW + margeX
let winH = margeY + plateauH + margeY

let tailleTexte = 20

let initGraph () =
  Graphics.open_graph "";
  Graphics.resize_window winW winH;
  Graphics.set_text_size tailleTexte

(* Renvoie la couleur associée à une case du plateau. *)
let couleurCase = function
  | Jeton Jaune -> coulJaune
  | Jeton Rouge -> coulRouge
  | Noir        -> coulVide

let pixelCase x y = (margeX+caseW*x, margeY+caseH*y)

let printCase case x y =
  let (px, py) = pixelCase x y in
  Graphics.set_color (couleurCase case);
  Graphics.fill_circle (px+caseW/2) (py+caseH/2) rayonJ

let printTour joueur =
  printCase (Jeton joueur) sizeX (sizeY-1)

let updateCase jeu x y =
  printCase jeu.pl.(x).(y) x y

let printJeu jeu =
  (* effacer l’écran *)
  (*Graphics.clear_graph ();*)
  Graphics.set_color coulFond;
  Graphics.fill_rect 0 0 winW winH;
  (* dessiner la grille en plein *)
  let (px,py) = pixelCase 0 0 in
  Graphics.set_color coulPlateau;
  Graphics.fill_rect px py plateauW plateauH;
  (* dessiner les cases *)
  for x = 0 to sizeX-1 do
    for y = 0 to sizeY-1 do
      updateCase jeu x y
    done
  done;
  (* écrire les numéros de colonne en-dessous du plateau *)
  for x = 1 to sizeX do
    let texte = string_of_int x in
    let (w,h) = Graphics.text_size texte in
    Graphics.set_color coulTexte;
    Graphics.moveto (margeX + x*caseW - (caseW+w)/2) (margeY - h - 2);
    Graphics.draw_string texte
  done


let lireChiffre min max =
  let k = ref (min-1) in
  while not (min <= !k && !k <= max) do
    let status = Graphics.wait_next_event [Graphics.Key_pressed] in
    k := (Char.code status.Graphics.key) - (Char.code '0')
  done;
  !k

let printMenu titre items =
  let items = Array.of_list items in
  let n = Array.length items in
  (* dessiner le menu à droite du plateau de jeu *)
  let x = margeX + plateauW + margeX
  and y = ref margeY in
  for i = n downto 1 do (* on écrit les éléments à l’envers (de bas en haut) *)
    let texte = snd items.(i-1) in
    let texte' = string_of_int i ^ ": " ^ texte in
    let (w,h) = Graphics.text_size texte' in
    Graphics.set_color coulMenuFond;
    Graphics.fill_rect x !y (w+2*margeMenuTexte) (h+2*margeMenuTexte);
    Graphics.set_color coulMenuTexte;
    Graphics.moveto (x+margeMenuTexte) (!y+margeMenuTexte);
    Graphics.draw_string texte';
    y := !y + h + margeMenuTexte + margeMenuItem
  done;
  (* afficher le titre *)
  Graphics.set_color coulTexte;
  Graphics.moveto x !y;
  Graphics.draw_string titre;
  (* attendre le choix de l’utilisateur *)
  let i = lireChiffre 1 n in
  (* effacer le menu (tout ce qu’il y a à droite du plateau) *)
  Graphics.set_color coulFond;
  Graphics.fill_rect x margeY menuW (plateauH + margeY);
  (* retourner la clé de l’élément choisi *)
  fst items.(i-1)

let printMessage msg =
  let (x,y) = pixelCase sizeX sizeY
  and (w,h) = Graphics.text_size msg in
  (* effacer tout ce qu’il y a à droite du plateau *)
  Graphics.set_color coulFond;
  Graphics.fill_rect x margeY (winW-x) plateauH;
  (* écrire le message en haut à droite *)
  Graphics.set_color coulTexte;
  Graphics.moveto (x+margeX) (y-h);
  Graphics.draw_string msg
let printfMessage fmt = Printf.ksprintf printMessage fmt
