(******************************************************************************)
(*               Puissance 4                                                  *)
(*                                                               Glen Mével   *)
(*                                                                            *)
(*    Définitions des données du jeu et des fonctions utilitaires             *)
(*                                                                            *)
(******************************************************************************)

type joueur = Jaune | Rouge
type case = Jeton of joueur | Noir
type plateau = case array array
type jeu = {
  pl : plateau;
  mutable tour : joueur;
}

let sizeX = 7
let sizeY = 6
let nbV = 4

let initJeu () = {
  pl = Array.make_matrix sizeX sizeY Noir;
  tour = Jaune;
}

let isFull jeu x = jeu.pl.(x).(sizeY-1) <> Noir

let hauteurColonne jeu x =
  let y = ref 0 in
  while !y < sizeY && jeu.pl.(x).(!y) <> Noir do
    incr y
  done;
  !y

let nombreJetons jeu =
  let s = ref 0 in
  for x = 0 to sizeX-1 do
    s := !s + hauteurColonne jeu x
  done;
  !s

let addJeton jeu x jeton =
  let y = hauteurColonne jeu x in
  jeu.pl.(x).(y) <- Jeton jeton;
  y

let cancelJeton jeu x y =
  jeu.pl.(x).(y) <- Noir

let joueurAdverse jeu =
  match jeu.tour with
  | Jaune -> Rouge
  | Rouge -> Jaune

let tourSuivant jeu =
  jeu.tour <- joueurAdverse jeu

let estGagnant jeu joueur x y =
  (* On va compter l’alignement maximal de pions passant par le pion joué
     et selon chaque direction. *)
  let count (a,b) =
    let k = ref 1 in
    while   0 <= !k*a+x  &&  !k*a+x < sizeX
        &&  0 <= !k*b+y  &&  !k*b+y < sizeY
        &&  jeu.pl.(!k*a+x).(!k*b+y) = Jeton joueur do
      incr k
    done;
    !k-1
  in
  (* horizontalement *)    count (-1, 0) + count (1, 0) + 1 >= nbV
  (* verticalement   *) || count ( 0,-1) + count (0, 1) + 1 >= nbV
  (* oblique \       *) || count (-1, 1) + count (1,-1) + 1 >= nbV
  (* oblique /       *) || count (-1,-1) + count (1, 1) + 1 >= nbV
