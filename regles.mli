(******************************************************************************)
(*               Puissance 4                                                  *)
(*                                                               Glen Mével   *)
(*                                                                            *)
(*    Définitions des données du jeu et des fonctions utilitaires             *)
(*                                                                            *)
(******************************************************************************)

type joueur = Jaune | Rouge            (* joueurs et ses jetons (Jaune commence) *)
type case = Jeton of joueur | Noir     (* case du plateau *)
type plateau = case array array        (* plateau de jeu *)
type jeu = {
  pl : plateau;                        (* indices : (col,ligne), bas-gauche : (0,0) *)
  mutable tour : joueur;               (* prochain tour *)
}

val sizeX: int                         (* nb. de colonnes *)
val sizeY: int                         (* nb. de lignes *)
val nbV: int                           (* condition de vicoire *)

(* Créer un nouveau jeu *)
val initJeu: unit -> jeu

(* Test si une colonne [x] est pleine dans [jeu] *)
val isFull: jeu -> int -> bool

(* Retourne le nombre de jetons dans la colonne [x]. *)
val hauteurColonne: jeu -> int -> int

(* Retourne le nomble total de jetons joués. *)
val nombreJetons: jeu -> int

(* Ajoute un [jeton] dans une colonne [x] dans un [jeu] et
   renvoie la ligne où a été placé le jeton.
   Suppose que la colonne n’est pas déjà pleine. *)
val addJeton: jeu -> int -> joueur -> int

(* Enlève le pion joué (sert pour l’IA). *)
val cancelJeton: jeu -> int -> int -> unit

(* Donne le joueur dont ce n’est pas le tour de jouer. *)
val joueurAdverse: jeu -> joueur

(* Change le tour de jeu. *)
val tourSuivant: jeu -> unit

(* Test si un [joueur] a gagné connaissant la position ([x],[y]) du dernier
   jeton joué (par ce joueur) *)
val estGagnant: jeu -> joueur -> int -> int -> bool
