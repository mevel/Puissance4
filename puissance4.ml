(******************************************************************************)
(*               Puissance 4                                                  *)
(*                                                               Glen Mével   *)
(*                                                                            *)
(*    Session interactive permettant à des joueurs humains et des IA de jouer *)
(*                                                                            *)
(******************************************************************************)

open Regles
open Ia
open Affiche

(* Convertit un jeton en texte. *)
let nomJoueur = function
  | Jaune -> "Jaune"
  | Rouge -> "Rouge"

type sourceCoups =
  | Humain
  | IA

let getCoupHumain jeu =
  lireChiffre 0 sizeX

let getCoup jeu = function
  | Humain -> getCoupHumain jeu
  | IA     -> getCoupIA jeu

exception FinJeu of case
exception Interrompu

let jouer sourceJoueur1 sourceJoueur2 =
  let jeu = initJeu ()
  and coups = ref (sizeX*sizeY) (* nombre de coups restant à jouer *) in
  printJeu jeu;
  printfMessage "    0: renoncer";
  try while true do
    if !coups = 0 then
      raise (FinJeu Noir);
    printTour jeu.tour;
    let col = getCoup jeu (if jeu.tour = Jaune then sourceJoueur1 else sourceJoueur2) in
    if col = 0 then
      raise Interrompu;
    let x = col-1 in
    if not (isFull jeu x) then begin
      let y = addJeton jeu x jeu.tour in
      updateCase jeu x y;
      if estGagnant jeu jeu.tour x y then
        raise (FinJeu (Jeton jeu.tour));
      decr coups;
      tourSuivant jeu
    end
  done with
  | FinJeu (Jeton joueur) -> printfMessage "Victoire de %s\xA0!" (nomJoueur joueur)
  | FinJeu Noir           -> printfMessage "Match nul."
  | Interrompu            -> printfMessage "Abandon de %s." (nomJoueur jeu.tour)

let main () =
  initGraph ();
  printJeu (initJeu ()); (* afficher un plateau vide pour l’écran d’accueil *)
  while
   printMenu "Puissance 4\xA0!"
    [ true,  "jouer";
      false, "quitter"; ]
  do
    let sourceJoueur1 =
     printMenu "Joueur 1 (Jaune)"
      [ Humain, "humain";
        IA,     "ordinateur"; ]
    in
    let sourceJoueur2 =
     printMenu "Joueur 2 (Rouge)"
      [ Humain, "humain";
        IA,     "ordinateur"; ]
    in
    jouer sourceJoueur1 sourceJoueur2
  done

let () = main ()
