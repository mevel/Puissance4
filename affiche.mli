(******************************************************************************)
(*               Puissance 4                                                  *)
(*                                                               Glen Mével   *)
(*                                                                            *)
(*    Affichage du plateau, menu, etc..                                       *)
(*                                                                            *)
(******************************************************************************)

(* Initialise la fenêtre graphique *)
val initGraph: unit -> unit

val coulFond:    Graphics.color
val coulPlateau: Graphics.color
val coulVide:    Graphics.color
val coulJaune:   Graphics.color
val coulRouge:   Graphics.color
val coulTexte:   Graphics.color
val coulMenuFond:  Graphics.color
val coulMenuTexte: Graphics.color

val margeX:   int
val margeY:   int
val caseW:    int
val caseH:    int
val rayonJ:   int
val plateauW: int
val plateauH: int
val menuW:    int
val margeMenuTexte: int
val margeMenuItem:  int
val winW:     int
val winH:     int

val tailleTexte: int

(* Renvoie le couple des coordonnées d'une case située en ([x],[y])
   sur le plateau. *)
val pixelCase: int -> int -> (int*int)

(* Affiche une case. *)
val printCase: Regles.case -> int -> int -> unit

(* Met à jour l’affichage de la case du plateau. *)
val updateCase: Regles.jeu -> int -> int -> unit

(* Affiche un jeton disant à qui c’est le tour de jouer. *)
val printTour: Regles.joueur -> unit

(* Affiche un jeu. *)
val printJeu: Regles.jeu -> unit


(* Lit un chiffre entre [min] et [max] au clavier. *)
val lireChiffre: int -> int -> int

(* Affiche un menu et attend le choix de l’utilisateur au clavier.
 * Renvoie la clé associée à l’élément élu. *)
val printMenu: string -> ('a * string) list -> 'a

(* Affiche un message (s’utilise comme printf). *)
val printfMessage: ('a, unit, string, unit) format4 -> 'a
